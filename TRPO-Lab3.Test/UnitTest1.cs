using NUnit.Framework;
using TRPO_Lab3_lib;
using System;


namespace Test_TRPO_Lab3
{
    public class Tests
    {

        [Test]
        public void Test1()
        {
            const int h = 2;
            const int P = 3;
            const int expected = 3;

            var result = new Class1().Square(h, P);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Test2()
        {
            const int h = -2;
            const int P = 3;

            Assert.Throws <ArgumentException> (()=> new Class1().Square(h,P));
        }
    }
}