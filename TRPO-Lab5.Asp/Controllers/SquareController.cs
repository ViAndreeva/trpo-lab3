﻿using Microsoft.AspNetCore.Mvc;
using TRPO_Lab5.Asp.Models;
using TRPO_Lab3_lib;
using Microsoft.AspNetCore.Http;
using System;

namespace TRPO_Lab5.Asp.Controllers
{
    public class SquareController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        //GET

        public ActionResult Edit(int P, int H, IFormCollection collection)
        {
            try
            {
                collection.TryGetValue(nameof(Container.P), out var p);
                collection.TryGetValue(nameof(Container.H), out var h);
                var vm = new Container();
                vm.P = Convert.ToInt32(p);
                vm.H = Convert.ToInt32(h);
                vm.Square =  new Class1().Square(H, P).ToString();
                return View(nameof(Index), vm);
            }
            catch
            {
                return RedirectToAction(nameof(HomeController.Error), "Home");
            }

        }

        private ActionResult View(string v, Container vm, string vm1)
        {
            throw new NotImplementedException();
        }
        //POST
    }

}
