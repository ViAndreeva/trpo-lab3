﻿using System.Windows;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System;
using TRPO_Lab3_lib;

namespace TRPO_Lab4
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new LibViewModel();
        }

        public class LibViewModel : INotifyPropertyChanged
        {
            private int p;
            private int h;

            public event PropertyChangedEventHandler PropertyChanged;

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }

            private class NotifyPropertyChangedInvocatorAttribute : Attribute
            {
            }
            public int P
            {
                get => p; set
                {
                    p = value;
                    OnPropertyChanged(nameof(p));
                    OnPropertyChanged(nameof(Square));
                }
            }

            public int H
            {
                get => h; set
                {
                    h = value;
                    OnPropertyChanged(nameof(H));
                    OnPropertyChanged(nameof(Square));
                }
            }

            public string Square
            {
                get
                {
                    try
                    {
                        return new Class1().Square(h, P).ToString();
                    }
                    catch (Exception ex)
                    {
                        return ex.Message;
                    }

                }
            }
            public LibViewModel()
            {
                p = 0;
                h = 0;
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

