﻿using System;
using TRPO_Lab3_lib;

namespace TRPO_Lab3
{
    class Program
    {  
        static void Main(string[] args)
        {
            Console.WriteLine("Введите апофему пирамиды");
            int h = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Введите периметр основания");
            int P = Convert.ToInt32(Console.ReadLine());
            try
            {
                var result = new Class1().Square(h, P);
                Console.WriteLine("Результат подсчета формулы " + result);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
